// JavaScript Document


(function(){
	"use strict";
	
	$('.toast').toast('show');
	
	var logSect = $('.modal-login-sect'),
		logCont = $('.login-continue'),
	 	logPass = $('.modal-login-pass'),
	 	logBtn = $('.login-btn'),
		backLog = $('.back-login'),
		modalRegWrap = $('.modal-signup-wrap'),
		modalLogWrap = $('.modal-login-wrap'),
		acctLog = $('.login-acct'),
		acctReg = $('.reg-acct');
	
	logCont.on('click', function(e){
		e.preventDefault;
		logSect.addClass('close-sect');
		logPass.removeClass('close-sect');
	});
	
	backLog.on('click', function(e){
		e.preventDefault;
		logSect.removeClass('close-sect');
		logPass.addClass('close-sect');
	});
	
	acctReg.on('click', function(e){
		e.preventDefault;
		modalLogWrap.addClass('close-sect');
		modalRegWrap.removeClass('close-sect');
	});
	
	acctLog.on('click', function(e){
		e.preventDefault;
		modalLogWrap.removeClass('close-sect');
		modalRegWrap.addClass('close-sect');
	});
	
}());